// require directive tells us to load the express module
const { response } = require('express');
const express = require('express');

// creating a server using express
const app = express();

// port
const port = 4000;

// middlewares
app.use(express.json())
    // allows app to read a json data

app.use(express.urlencoded({ extended: true }))
    // allows app to read data from forms
    // by default, information received from url can only be received as string or an array.
    // with extended:true, this allows to receive information in other data types such as objects.


// mock database
let users = [{
        email: "nezukoKamado@gmail.com",
        username: "nezuko01",
        password: "letMeOut",
        isAdmin: false
    },
    {
        email: "tanjiroKamado@gmail.com",
        username: "gonpanchiro",
        password: "iAmTanjiro",
        isAdmin: false
    },
    {
        email: "zenitsuAgatsuma@gmail.com",
        username: "zenitsuSleeps",
        password: "iNeedNezuko",
        isAdmin: true
    }
]
let loggedUser;

// GET method
app.get('/hello', (req, res) => {
    res.send('Hello from Batch 131')
});

// app - server
// get - HTTP method
// '/' - route name or endpoint
// (req,res) - request and response- will handle the requests and responses,
// res.send - combines writeHead() and end(), used to send response to our client.

// POST METHOD
app.post('/', (req, res) => {
    console.log(req.body);
    res.send(`Hello I am ${req.body.name}, I am ${req.body.age}, I could be described as ${req.body.description}`)
});

app.post('/users/register', (req, res) => {
    console.log(req.body);

    let newUser = {
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        isAdmin: req.body.isAdmin
    }

    users.push(newUser);
    console.log(users);

    res.send(`User ${req.body.username} has successfully been registered.`)
})

// login route
app.post('/users/login', (req, res) => {
    console.log(req.body);

    let foundUser = users.find((user) => {
        return user.username === req.body.username && user.password === req.body.password;
    });

    if (foundUser !== undefined) {
        let foundUserIndex = users.findIndex((user) => {
            return user.username === foundUser.username
        });

        foundUser.findIndex = foundUserIndex;

        loggedUser = foundUser
        console.log(foundUser)

        res.send(`Thank you for loggin in. ${req.body.username}`);
    } else {
        loggedUser = foundUser;

        res.send(`Login failed, wrong credentials`);
    }
});

// Change Password ROute
app.put('/users/change-password', (req, res) => {
    // store the message that will be sent back to our client
    var message;

    // will lopp through all the "users" array.
    for (let i = 0; i < users.length; i++) {

        // if the username provided in the request is the same with the username in the loop.
        if (req.body.username === users[i].username) {

            //change the password of the user found in the loop by the requested password inthe body by the client.
            users[i] = req.body.password;

            // send a message to client.
            message = `User ${req.body.username}'s password has been changed.`;

            // breaks the loop. once user matches username provided in the client.
            break;
        } else {

            // changes the message to be sent back as a response.
            message = `User not found.`;
        }

    }
    res.send(`${message}`);
});

// use console.log to check what's inside the request body.

// listens to the port and returning a message to the terminal.
app.listen(port, () => console.log(`Server is running at port ${port}`));

// Activity

// GET ROUTE
app.get('/home', (req, res) => {
    res.send('Welcome to our home.')
});

// GET Route that will retrieve all users in mock database.
app.get('/users', (req, res) => {
    res.send(users);
});

/* // DELETE USER from mock database
app.delete('/delete-user', (req, res) => {
    // store the message that will be sent back to our client
    var message;

    // will lopp through all the "users" array.
    for (let i = 0; i < users.length; i++) {

        // if the username provided in the request is the same with the username in the loop.
        if (req.body.username === users[i].username) {

            //change the password of the user found in the loop by the requested password inthe body by the client.
            users[i] = req.body.username;

            // send a message to client.
            message = `User ${req.body.username} has been deleted.`;

            // breaks the loop. once user matches username provided in the client.
            break;
        } else {

            // changes the message to be sent back as a response.
            message = `User not found.`;
        }

    }
    users.pop(req.body.username);
    console.log(users)
    res.send(`${message}`);
}) */